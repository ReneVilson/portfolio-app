import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import axios from 'axios'

Vue.config.productionTip = false
axios.defaults.baseURL = process.env.BACKEND_API_DOMAIN || "https://renevilson.online/api/v1"

new Vue({
    router,
    store,
    axios,
    vuetify,
    render: h => h(App)
}).$mount('#app')
